import pickle
from pathlib import Path

import click
import pandas as pd
from sklearn.metrics import f1_score


@click.command()
@click.option("--input-filepath", type=click.Path(path_type=Path, exists=True))
@click.option("--model-filepath", type=click.Path(path_type=Path))
def main(input_filepath: Path, model_filepath: Path) -> None:
    train_df = pd.read_csv(input_filepath)
    feature_cols = [
        "Age",
        "Gender",
        "family_history",
        "benefits",
        "care_options",
        "anonymity",
        "leave",
        "work_interfere",
    ]
    X = train_df[feature_cols]
    y = train_df.treatment
    model = pickle.load(open(model_filepath, "rb"))
    y_predict = model.predict(X)
    print(f"F1-score {f1_score(y, y_predict)}")


if __name__ == "__main__":
    main()
