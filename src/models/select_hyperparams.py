import pickle
from pathlib import Path

import click
import optuna
import pandas as pd
from optuna.integration import OptunaSearchCV
from xgboost import XGBClassifier


@click.command()
@click.option("--input-filepath", type=click.Path(path_type=Path, exists=True))
@click.option("--params-filepath", type=click.Path(path_type=Path))
def main(input_filepath: Path, params_filepath: Path) -> None:
    train_df = pd.read_csv(input_filepath)
    feature_cols = [
        "Age",
        "Gender",
        "family_history",
        "benefits",
        "care_options",
        "anonymity",
        "leave",
        "work_interfere",
    ]
    X = train_df[feature_cols]
    y = train_df.treatment
    param = {
        "learning_rate": optuna.distributions.FloatDistribution(0.01, 0.1, log=True),
        "n_estimators": optuna.distributions.IntDistribution(50, 150),
        "max_depth": optuna.distributions.IntDistribution(3, 5),
        "min_child_weight": optuna.distributions.IntDistribution(1, 3),
        "subsample": optuna.distributions.FloatDistribution(0.8, 1.0),
        "colsample_bytree": optuna.distributions.FloatDistribution(0.8, 1.0),
        "gamma": optuna.distributions.FloatDistribution(0, 0.1),
    }

    # Создание объекта OptunaSearchCV
    optuna_search = OptunaSearchCV(
        XGBClassifier(
            objective="binary:logistic",
            use_label_encoder=False,
            random_state=42,
        ),
        param,
        scoring="f1_weighted",
        n_trials=100,
        cv=3,
        verbose=1,
        random_state=42,
    )

    # Запуск поиска
    optuna_search.fit(X, y)

    print("Наилучшие параметры:", optuna_search.best_params_)
    print("Наилучший показатель качества (F1-мера):", optuna_search.best_score_)
    pickle.dump(optuna_search.best_params_, open(params_filepath, "wb"))


if __name__ == "__main__":
    main()
