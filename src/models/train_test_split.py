from pathlib import Path

import click
import pandas as pd
from sklearn.model_selection import train_test_split


@click.command()
@click.option("--input-filepath", type=click.Path(path_type=Path, exists=True))
@click.option("--output-filepath", type=click.Path(path_type=Path))
@click.option("--test-size", type=click.FLOAT, default=0.2)
def main(input_filepath: Path, output_filepath: Path, test_size: float) -> None:
    output_filepath.mkdir(exist_ok=True, parents=True)
    df = pd.read_csv(input_filepath)
    train, test = train_test_split(df, test_size=test_size)
    train.to_csv(output_filepath / "train.csv", index=False)
    test.to_csv(output_filepath / "test.csv", index=False)


if __name__ == "__main__":
    main()
