import pickle
from pathlib import Path

import click
import mlflow
import pandas as pd
from mlflow.pyfunc import PythonModel, PythonModelContext
from xgboost import XGBClassifier


class ModelWrapper(PythonModel):
    def load_context(self, context: PythonModelContext):

        self._encoder = pickle.load(open(context.artifacts["encoder"], "rb"))
        self._model = pickle.load(open(context.artifacts["model"], "rb"))

    def predict(self, context: PythonModelContext, data):
        for d in data.columns:
            data[d] = self._encoder[d].transform(data[d])
        return self._model.predict(data)


@click.command()
@click.option("--input-filepath", type=click.Path(path_type=Path, exists=True))
@click.option("--params-filepath", type=click.Path(path_type=Path, exists=True))
@click.option("--output-filepath", type=click.Path(path_type=Path))
def main(input_filepath: Path, params_filepath: Path, output_filepath: Path) -> None:
    with mlflow.start_run():
        train_df = pd.read_csv(input_filepath)
        feature_cols = [
            "Age",
            "Gender",
            "family_history",
            "benefits",
            "care_options",
            "anonymity",
            "leave",
            "work_interfere",
        ]
        X = train_df[feature_cols]
        y = train_df.treatment
        params = pickle.load(open(params_filepath, "rb"))
        mlflow.log_params(params)
        eval_set = [(X, y)]
        xgb_model = XGBClassifier(
            objective="binary:logistic",
            use_label_encoder=False,
            random_state=42,
            **params
        )
        xgb_model.fit(
            X, y, eval_set=eval_set, eval_metric=["logloss", "auc"], verbose=True
        )
        results = xgb_model.evals_result()
        train_logloss = results["validation_0"]["logloss"]
        train_aucs = results["validation_0"]["auc"]
        for train_loss, train_auc in zip(train_logloss, train_aucs):
            mlflow.log_metric("Train logloss", train_loss)
            mlflow.log_metric("Train AUC", train_auc)
        pickle.dump(xgb_model, open(output_filepath, "wb"))
        mlflow.pyfunc.log_model(
            "classifier",
            python_model=ModelWrapper(),
            artifacts={"encoder": "encoders.pkl", "model": str(output_filepath)},
            registered_model_name="classifier",
        )


if __name__ == "__main__":
    main()
