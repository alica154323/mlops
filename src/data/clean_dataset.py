# -*- coding: utf-8 -*-
import logging
from pathlib import Path

import click
import pandas as pd
from dotenv import find_dotenv, load_dotenv


def _fill_missing_values(df: pd.DataFrame) -> pd.DataFrame:
    defaultInt = 0
    defaultString = "NaN"
    defaultFloat = 0.0

    # Create lists by data tpe
    intFeatures = ["Age"]
    stringFeatures = [
        "Gender",
        "Country",
        "self_employed",
        "family_history",
        "treatment",
        "work_interfere",
        "no_employees",
        "remote_work",
        "tech_company",
        "anonymity",
        "leave",
        "mental_health_consequence",
        "phys_health_consequence",
        "coworkers",
        "supervisor",
        "mental_health_interview",
        "phys_health_interview",
        "mental_vs_physical",
        "obs_consequence",
        "benefits",
        "care_options",
        "wellness_program",
        "seek_help",
    ]
    floatFeatures: list[str] = []

    for feature in df:
        if feature in intFeatures:
            df[feature] = df[feature].fillna(defaultInt)
        elif feature in stringFeatures:
            df[feature] = df[feature].fillna(defaultString)
        elif feature in floatFeatures:
            df[feature] = df[feature].fillna(defaultFloat)
        else:
            print("Error: Feature %s not recognized." % feature)
    return df


def _clean_gender(df):
    male_str = [
        "male",
        "m",
        "male-ish",
        "maile",
        "mal",
        "male (cis)",
        "make",
        "male ",
        "man",
        "msle",
        "mail",
        "malr",
        "cis man",
        "Cis Male",
        "cis male",
    ]
    trans_str = [
        "trans-female",
        "something kinda male?",
        "queer/she/they",
        "non-binary",
        "nah",
        "all",
        "enby",
        "fluid",
        "genderqueer",
        "androgyne",
        "agender",
        "male leaning androgynous",
        "guy (-ish) ^_^",
        "trans woman",
        "neuter",
        "female (trans)",
        "queer",
        "ostensibly male, unsure what that really means",
    ]
    female_str = [
        "cis female",
        "f",
        "female",
        "woman",
        "femake",
        "female ",
        "cis-female/femme",
        "female (cis)",
        "femail",
    ]

    gender_map = {gender: "male" for gender in male_str}
    gender_map.update({gender: "female" for gender in female_str})
    gender_map.update({gender: "trans" for gender in trans_str})

    df["Gender"] = df["Gender"].str.lower().map(gender_map).fillna(df["Gender"])

    stk_list = ["A little about you", "p"]
    df = df[~df["Gender"].isin(stk_list)]

    return df


def _clean_age(df: pd.DataFrame):
    median_age = df["Age"].median()
    df["Age"].fillna(median_age, inplace=True)
    df.loc[df["Age"] < 18, "Age"] = median_age
    df.loc[df["Age"] > 120, "Age"] = median_age
    return df


def _fix_columns(df: pd.DataFrame):
    default_string = "NaN"
    df["self_employed"] = df["self_employed"].replace(default_string, "No")
    df["work_interfere"] = df["work_interfere"].replace(default_string, "Don't know")
    return df


@click.command()
@click.option("--input-filepath", type=click.Path(exists=True, path_type=Path))
@click.option("--output-filepath", type=click.Path(path_type=Path))
def main(input_filepath: Path, output_filepath: Path):
    logger = logging.getLogger(__name__)
    df = pd.read_csv(input_filepath)
    df = _fill_missing_values(df)
    df = _clean_gender(df)
    df = _clean_age(df)
    df = _fix_columns(df)
    df.to_csv(output_filepath, index=False)
    logger.info("making final data set from raw data")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
