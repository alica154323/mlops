import logging
from pathlib import Path

import click
from dotenv import find_dotenv, load_dotenv
from kaggle import api


@click.command()
@click.option("--dataset-name", type=str)
@click.option("--output-filepath", type=click.Path(path_type=Path))
def main(dataset_name: str, output_filepath: Path) -> None:
    """Download raw dataset from kaggle."""
    logger = logging.getLogger(__name__)
    api.dataset_download_files(
        dataset=dataset_name, path=output_filepath, quiet=False, unzip=True
    )
    logger.info("finish download dataset")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    load_dotenv(find_dotenv())
    api.authenticate()

    main()
