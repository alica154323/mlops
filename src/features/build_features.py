import logging
import pickle
from pathlib import Path

import click
import pandas as pd
from dotenv import find_dotenv, load_dotenv
from sklearn.preprocessing import LabelEncoder


def _add_age_range(df: pd.DataFrame) -> pd.DataFrame:
    df["age_range"] = pd.cut(
        df["Age"],
        [0, 20, 30, 65, 100],
        labels=["0-20", "21-30", "31-65", "66-100"],
        include_lowest=True,
    )
    return df


def _encode_data(df: pd.DataFrame) -> pd.DataFrame:
    encoders = {}
    for feature in df:
        le = LabelEncoder()
        le.fit(df[feature])
        df[feature] = le.transform(df[feature])
        encoders[feature] = le
    pickle.dump(encoders, open("encoders.pkl", "wb"))
    return df


def _drop_country(df: pd.DataFrame) -> pd.DataFrame:
    df = df.drop(["Country"], axis=1)
    return df


@click.command()
@click.option("--input-filepath", type=click.Path(path_type=Path, exists=True))
@click.option("--output-filepath", type=click.Path(path_type=Path))
def main(input_filepath: Path, output_filepath: Path) -> None:
    logger = logging.getLogger(__name__)
    df = pd.read_csv(input_filepath)
    df = _add_age_range(df)
    df = _encode_data(df)
    df = _drop_country(df)
    df.to_csv(output_filepath, index=False)
    logger.info("finish feature building")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    load_dotenv(find_dotenv())
    main()
