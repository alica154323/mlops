import pickle
from pathlib import Path

import click
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.inspection import permutation_importance


@click.command()
@click.option("--model-filepath", type=click.Path(path_type=Path, exists=True))
@click.option("--test-filepath", type=click.Path(path_type=Path))
@click.option("--graph-filepath", type=click.Path(path_type=Path))
def main(model_filepath: Path, test_filepath: Path, graph_filepath: Path):
    graph_filepath.mkdir(exist_ok=True, parents=True)
    train_df = pd.read_csv(test_filepath)
    feature_cols = [
        "Age",
        "Gender",
        "family_history",
        "benefits",
        "care_options",
        "anonymity",
        "leave",
        "work_interfere",
    ]
    X = train_df[feature_cols]
    y = train_df.treatment

    model = pickle.load(open(model_filepath, "rb"))

    sorted_idx = model.feature_importances_.argsort()
    plt.barh(X.columns[sorted_idx], model.feature_importances_[sorted_idx])
    plt.xlabel("Xgboost Feature Importance")
    plt.savefig(graph_filepath / "feature_imp.png")

    perm_importance = permutation_importance(model, X, y)
    sorted_idx = perm_importance.importances_mean.argsort()
    plt.barh(X.columns[sorted_idx], perm_importance.importances_mean[sorted_idx])
    plt.xlabel("Permutation Importance")
    plt.savefig(graph_filepath / "perm_importance.png")


if __name__ == "__main__":
    main()
